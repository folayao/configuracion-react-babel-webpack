const path = require('path'); /* Modulo existente en node */
const HtmlWebpackPlugin = require('html-webpack-plugin'); /* plugin instalado
 */

module.exports = {
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'dist'),/* Archivos resultantes de la compilación */
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', ',jsx'] /* Resolvemos las extenciones a usar en nuestro proyecto */
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.html$/,
                use: [
                    {
                        loader: 'html-loader'
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html',
            filename: './index.html'
        })
    ]
}
